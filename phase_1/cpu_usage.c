#include<stdio.h>
#include<unistd.h>

int main()
{
	double first_cpu_waste = 0, second_cpu_waste = 0;
	double first_cpu_used = 0, second_cpu_used = 0;

	char str[500];

	FILE* desc;

	while(1)
	{
		desc = fopen("/proc/stat", "r"); //open file
		fscanf(desc, "%s", str);

		if(str[0] == 'c' && str[1] == 'p' && str[2] == 'u' && str[3] == 0) //if input == cpu
		{
			int i;
			long long temp;
			for(i = 0; i < 3; i++)
			{
				fscanf(desc, "%lld", &temp);
				second_cpu_used += temp; //first 4 numbers are well used cpu times
			}
			for(i = 0; i < 2; i++)
			{
				fscanf(desc, "%lld", &temp);
				second_cpu_waste += temp; //next 2 numbers are wasted cpu times
			}
			for(i = 0; i < 5; i++)
			{
				fscanf(desc, "%lld", &temp);
				second_cpu_used += temp; //well used cpu times
			}

			double cpu_usage = ((double)(second_cpu_used - first_cpu_used)) / (double)((second_cpu_used - first_cpu_used) + (second_cpu_waste - first_cpu_waste)) * 100.0; // gets cpu usage percentage
			printf("%6.2f%%\r", cpu_usage);

			first_cpu_used = second_cpu_used;
			first_cpu_waste = second_cpu_waste;
		}
		fclose(desc); //closes the opened file
	}
	return 0;
}